'use strict';

var DayLogModel = require('../model/daylog.model');
var constants = require('../config/constants');

var DayLogController = {
    // TODO: Check if this validation method should be in a Controller or other config file
    validateSlug: (req, res, next, slug) => {
        DayLogModel.findOne({ "slug": slug },
            (err, daylog) => {
                if (err || daylog === null) {
                    console.error('DayLog with slug <' + slug + '> was not found');
                    res.status(404)
                    var err = new Error('Not Found');
                    err.status = 404;

                    res.format({
                        html: () => { next(err); },
                        json: () => { res.json({message : err.status  + ' ' + err}); }
                    });
                } else {
                    req.slug = slug;
                    next();
                }
            }
        );
    },

    index: (req, res) => {
        DayLogModel.find({},
            (err, daylogs) => {
                if(err) return console.error(err);

                res.format({
                    html: () => {
                        res.render('index', {
                            daylogs: daylogs
                        });
                    },
                    json: () => { res.json(daylogs); }
                })
            }
        );
    },

    create: (req, res) => {
        res.render('daylogs/form', {
            operation: constants.FORM_OPERATIONS.create,
            constants: constants,
            action: '/store',
            method: constants.HTTP_METHODS.post
        });
    },

    store: (req, res) => {
        var title = req.body.title;
        var slug = req.body.slug;
        var location = req.body.location;
        var logAt = req.body.logAt;
        var category = req.body.category;

        DayLogModel.create({
                title : title,
                slug : slug,
                location : location,
                logAt : logAt,
                category : category
            }, (err, daylog) => {
                if(err) res.send("There was a problem adding the information to the database: " + err);

                console.log('Creating: "' + daylog.title + '"');

                res.format({
                    html: () => {
                        res.redirect("/" + daylog.slug);
                    },
                    json: () => { res.json(daylog); }
                }
            );
        });
    },

    show: (req, res) => {
        // TODO: Find other way to get slug instead of using arbitrary array splitting
        DayLogModel.findOne({ "slug": req.url.split("/")[1] },
            (err, daylog) => {
                if(err) console.error('GET Error: There was a problem retrieving: ' + err);

                console.info('Show: "' + daylog.title + '"');

                res.format({
                    html: () => {
                        res.render('daylogs/form', {
                            daylog : daylog,
                            operation: constants.FORM_OPERATIONS.view,
                            constants: constants,
                            action: null,
                            method: null
                        });
                    },
                    json: () => { res.json(daylog); }
                });
            }
        );
    },

    edit: (req, res) => {
        DayLogModel.findOne({ "slug": req.url.split("/")[1] },
            (err, daylog) => {
                if(err) return console.error('GET Error: There was a problem retrieving: ' + err);

                res.format({
                    html: () => {
                        res.render('daylogs/form', {
                            daylog : daylog,
                            operation: constants.FORM_OPERATIONS.edit,
                            constants: constants,
                            action: '/' + daylog.slug + '/edit',
                            method: constants.HTTP_METHODS.post
                        });
                    },
                    json: () => { res.json(daylog); }
                });
            }
        );
    },

    update: (req, res) => {
        var title = req.body.title;
        var slug = req.body.slug;
        var location = req.body.location;
        var logAt = req.body.logAt;
        var category = req.body.category;
        var updatedAt = Date.now();

        DayLogModel.findOne({ "slug": req.body.slug },
            (err, daylog) => {
                daylog.update({
                    title: title,
                    slug: slug,
                    location: location,
                    logAt: logAt,
                    category: category,
                    updatedAt: updatedAt
                }, (err, slug) => {
                    if(err) res.send("There was a problem updating the information to the database: " + err);

                    console.info('Editing: "' + daylog.title + '"');

                    res.format({
                        html: () => { res.redirect("/" + daylog.slug); },
                        json: () => { res.json(daylog); }
                    });
                })
            }
        );
    },

    delete: (req, res) => {
        DayLogModel.findOne({ "slug": req.url.split("/")[1] },
            (err, daylog) => {
                if(err) return console.error(err);

                daylog.remove((err, daylog) => {
                    if(err) return console.error(err);

                    console.info('Deleting: "' + daylog.title + '"');

                    res.format({
                        html: () => { res.redirect("/"); }
                    });
                });
            }
        );
    }
};

module.exports = DayLogController;
