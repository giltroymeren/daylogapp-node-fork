# DayLogApp (node.js)

This application was developed form the combination of the following tutorials:

- Kendrick Coleman's "[How to Create a Complete Express.js + Node.js + MongoDB CRUD and REST Skeleton](https://www.airpair.com/javascript/complete-expressjs-nodejs-mongodb-crud-skeleton)"
- Siddhesh Mangela's "[airpairApp](https://github.com/siddacool/airpair-crud-app)"
- Zell Liew's "[Building a Simple CRUD Application with Express and MongoDB](https://zellwk.com/blog/crud-express-mongodb/)"
- Raja Sekar's "[A TDD Approach to Building a Todo API Using Node.js and MongoDB](https://semaphoreci.com/community/tutorials/a-tdd-approach-to-building-a-todo-api-using-node-js-and-mongodb)"

