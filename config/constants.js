'use strict';

/**
 * @source http://stackoverflow.com/a/21247500
 */
module.exports = Object.freeze({
    FORM_OPERATIONS: {
        create: 'Create',
        edit: 'Edit',
        view: 'View'
    },

    HTTP_METHODS: {
        post: 'post'
    }
});
