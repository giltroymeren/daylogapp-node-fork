'use strict';

var express = require('express');
var router = express.Router();
var database = require('../model/database');

var DayLogModel = require('../model/daylog.model');
var DayLogController = require('../controller/daylog.controller');

router.param('slug', DayLogController.validateSlug);

router.get('/', DayLogController.index);

router.get('/create', DayLogController.create);

router.post('/store', DayLogController.store);

router.get('/:slug', DayLogController.show);

router.get('/:slug/edit', DayLogController.edit);

router.put('/:slug/edit', DayLogController.update);

router.delete('/:slug/delete', DayLogController.delete);

module.exports = router;
