'use strict';

var mongoose = require('mongoose');

var databaseURL = 'mongodb://localhost/daylogapp';

mongoose.connect(databaseURL);
mongoose.connection.on('connection', () => {
    console.info('Mongoose default connection open to: ' + databaseURL);
});

module.exports = mongoose;
