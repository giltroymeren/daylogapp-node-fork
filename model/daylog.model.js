'use strict';

// TODO: Add unique identifier to 'slug' and 'logAt'

var mongoose = require('mongoose');
var DayLogSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    slug: {
        type: String,
        required: true,
    },
    location: {
        type: String,
        required: true,
    },
    logAt: {
        type: Date,
        required: true,
    },
    category: {
        type: String,
        enum: ['Adequate', 'Minor', 'Major'],
        required: true,
    },
    updatedAt: {
        type: Date,
        default: Date.now,
    }
});

var DayLogModel = mongoose.model('DayLog', DayLogSchema);

module.exports = DayLogModel;